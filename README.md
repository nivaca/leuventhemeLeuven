# beamerthemeNivaca
Nivaca Beamer Theme

Copyright 2017 Nicolas Vaughan (nivaca at fastmail dot com)

This file may be distributed and/or modified

1. under the LaTeX Project Public License and/or
2. under the GNU Public License.

This LaTeX theme was losely inspired by Fabrice Niessen's beautiful Emacs theme [leuven](https://github.com/fniessen/emacs-leuven-theme).


![First page](/images/01.png)

![Second page](/images/02.png)
